// Auto slider //

const swiper = new Swiper(".autoSlider", {
    slidesPerView: 3,
    loop: true,
    slidesPerView: 3,
    spaceBetween: 10,

});

// Burger modal menu //

const burger = document.querySelector('.burger__button');
const modal_menu = document.querySelector('.header-modal__menu');
const modal_close = document.querySelector('.header-modal__close');

burger.addEventListener('click', () => {
    modal_menu.classList.toggle('header-modal__menu--active');
    burger_span.classList.remove('burger--active');
})

modal_close.addEventListener('click', () => {
  modal_menu.classList.remove('header-modal__menu--active');
  
})

// Burger main menu //

const main_burger_button = document.querySelector('.burger__menu');
const main_burger_menu = document.querySelector('.header-main-burger__menu');
const section_background = document.querySelector('.section__taxi-license');
const burger_span = document.querySelector('.burger');
const burger_active = document.querySelector('.burger--active');
const burger_close = document.querySelector('.burger--close');

main_burger_button.addEventListener('click', () => {
    main_burger_menu.classList.toggle('header-main-burger__menu--active');
    modal_menu.classList.remove('header-modal__menu--active');
    burger_span.classList.toggle('burger--active');
})

burger.addEventListener('click', () => {
  main_burger_menu.classList.remove('header-main-burger__menu--active');
})

// Modal window //

function callbackModal(callback_button, callback_modal, callback_close, header, main, footer) {
    callback_button = document.querySelectorAll(callback_button),
    callback_modal = document.querySelector(callback_modal),
    callback_close = document.querySelector(callback_close),
    header = document.querySelector(header),
    main = document.querySelector(main),
    footer = document.querySelector(footer)
  
    btns = callback_button;
    btns.forEach(function (el) {
      el.addEventListener('click', () => {
        callback_modal.classList.toggle('footer-callback__modal--active');
        header.style.opacity = '0.3';
        main.style.opacity = '0.3';
        footer.style.opacity = '0.3';
      });
    })
  
    callback_close.addEventListener('click', () => {
      callback_modal.classList.remove('footer-callback__modal--active');
        header.style.opacity = '1';
        main.style.opacity = '1';
        footer.style.opacity = '1';

    });
  
    callback_modal.addEventListener('click', (e) => {
      if (e.target == callback_modal) {
        callback_modal.classList.remove('footer-callback__modal--active');
        header.style.opacity = '1';
        main.style.opacity = '1';
        footer.style.opacity = '1';
      }
    })
  }
  
callbackModal('.callback-mod_button', '.footer-callback__modal', '.footer-modal__close', '.header', '.main', '.footer')

// Modal form //

const button_after = document.querySelector('.button__after');
const title_after = document.querySelector('.footer-callback__after');
const content_before = document.querySelector('.footer-callback__before');

button_after.addEventListener('click', () => {
    content_before.style.display = 'none';
    title_after.style.display = 'flex';
})

// ScrollUp //

$(function() {
    $('.scrollup').click(function() {
      $("html, body").animate({
        scrollTop:0
      },1000);
    })
  })
  $(window).scroll(function() {
    if ($(this).scrollTop()>200) {
      $('.scrollup').fadeIn();
    }
    else {
      $('.scrollup').fadeOut();
    }
});

// Validate Form //

// Phone //

window.addEventListener("DOMContentLoaded", function() {
  [].forEach.call( document.querySelectorAll('.phone__input'), function(input) {
  var keyCode;
  function mask(event) {
      event.keyCode && (keyCode = event.keyCode);
      var pos = this.selectionStart;
      if (pos < 3) event.preventDefault();
      var matrix = "+7 (___) ___ ____",
          i = 0,
          def = matrix.replace(/\D/g, ""),
          val = this.value.replace(/\D/g, ""),
          new_value = matrix.replace(/[_\d]/g, function(a) {
              return i < val.length ? val.charAt(i++) || def.charAt(i) : a
          });
      i = new_value.indexOf("_");
      if (i != -1) {
          i < 5 && (i = 3);
          new_value = new_value.slice(0, i)
      }
      var reg = matrix.substr(0, this.value.length).replace(/_+/g,
          function(a) {
              return "\\d{1," + a.length + "}"
          }).replace(/[+()]/g, "\\$&");
      reg = new RegExp("^" + reg + "$");
      if (!reg.test(this.value) || this.value.length < 5 || keyCode > 47 && keyCode < 58) this.value = new_value;
      if (event.type == "blur" && this.value.length < 5)  this.value = ""
  }

  input.addEventListener("input", mask, false);
  input.addEventListener("focus", mask, false);
  input.addEventListener("blur", mask, false);
  input.addEventListener("keydown", mask, false)

});

});

// Send //

const form = document.forms["form"];
const formArr = Array.from(form);
const validFormArr = [];
const button = document.querySelector(".callback-form__button");

formArr.forEach((el) => {
  if (el.hasAttribute("data-reg")) {
    el.setAttribute("is-valid", "0");
    validFormArr.push(el);
  }
});

form.addEventListener("input", inputHandler);
button.addEventListener("click", buttonHandler);

function inputHandler({ target }) {
  if (target.hasAttribute("data-reg")) {
    inputCheck(target);
  }
}

function inputCheck(el) {
  const inputValue = el.value;
  const inputReg = el.getAttribute("data-reg");
  const reg = new RegExp(inputReg);
  if (reg.test(inputValue)) {
    el.setAttribute("is-valid", "1");
    el.style.color = "#24222D";
    el.style.border = "1px solid #fff";

  } else {
    el.setAttribute("is-valid", "0");
    el.style.color = "#DD3E3E";
    el.style.border = "1px solid #DD3E3E";
  }
}

function buttonHandler(e) {
  const allValid = [];
  validFormArr.forEach((el) => {
    allValid.push(el.getAttribute("is-valid"));
    console.log(allValid)
    
  });
  const isAllValid = allValid.reduce((acc, current) => {
    return acc && current;
  });

  if (!Boolean(Number(isAllValid))) {
    e.preventDefault();
  }
}

const accordionHeaders = document.querySelectorAll(".accordion__header");

ActivatingFirstAccordion();

function ActivatingFirstAccordion() {
    accordionHeaders[0].parentElement.classList.add("active");
    accordionHeaders[0].nextElementSibling.style.maxHeight =
        accordionHeaders[0].nextElementSibling.scrollHeight + "px";
}

function toggleActiveAccordion(e, header) {
    const activeAccordion = document.querySelector(".accordion.active");
    const clickedAccordion = header.parentElement;
    const accordionBody = header.nextElementSibling;

    if (activeAccordion && activeAccordion != clickedAccordion) {
        activeAccordion.querySelector(".accordion__body").style.maxHeight = 0;
        activeAccordion.classList.remove("active");
    }

    clickedAccordion.classList.toggle("active");

    if (clickedAccordion.classList.contains("active")) {
        accordionBody.style.maxHeight = accordionBody.scrollHeight + "px";
    } else {
        accordionBody.style.maxHeight = 0;
    }
}

accordionHeaders.forEach(function(header) {
    header.addEventListener("click", function(event) {
        toggleActiveAccordion(event, header);
    });
});

function resizeActiveAccordionBody() {
    const activeAccordionBody = document.querySelector(
        ".accordion.active .accordion__body"
    );
    if (activeAccordionBody)
        activeAccordionBody.style.maxHeight =
        activeAccordionBody.scrollHeight + "px";
}

window.addEventListener("resize", function() {
    resizeActiveAccordionBody();
});

//// Animation Car-1 ///

var animation;
gsap.registerPlugin(MotionPathPlugin, ScrollTrigger);
gsap.set("#motionSVG", { scale: 0.9, autoAlpha: 1 });

animation = gsap.to("#motionSVG", {
  scrollTrigger: {
    trigger: "#motionPath",
    start: "top 79%",
    end: '+=2000px',
    scrub: 50,
    markers: false,
    directional: true,
    normalizeScroll: false,
    animation: 1,
    effects: true,   
    smoothTouch: 0.1, 
  },

  inertia: false,
  ease: "none",
  immediateRender: false,
  motionPath: {
    path: "#motionPath",
    align: "#motionPath",
    alignOrigin: [0.5, 0.5],
    autoRotate: 175,
  }
});

//// Animation Car-2 ///

var animation;

gsap.registerPlugin(MotionPathPlugin, ScrollTrigger);
gsap.set("#motionSVG2", { scale: 0.9, autoAlpha: 1 });

animation = gsap.to("#motionSVG2", {
  scrollTrigger: {
    trigger: "#motionPath2",
    start: "top 110%",
    end: '+=2000px',
    scrub: 30,
    markers: true,
  },

  duration: 10,
  ease: "none",
  immediateRender: true,
  motionPath: {
    path: "#motionPath2",
    align: "#motionPath2",
    alignOrigin: [0.5, 0.5],
    autoRotate: 175,
  }
});

//// Animation Car-3 ///

var animation;

gsap.registerPlugin(MotionPathPlugin, ScrollTrigger);

gsap.set("#motionSVG3", { scale: 0.9, autoAlpha: 1 });

animation = gsap.to("#motionSVG3", {
  scrollTrigger: {
    trigger: "#motionPath3",
    start: "top 250%",
    end: '+=2000px',
    scrub: 40,
    markers: true,

  },
  duration: 10,
  ease: "none",
  immediateRender: true,
  motionPath: {
    path: "#motionPath3",
    align: "#motionPath3",
    alignOrigin: [0.5, 0.5],
    autoRotate: 175,
  }
});

//// Animation Car-4 ///

var animation;

gsap.registerPlugin(MotionPathPlugin, ScrollTrigger);
gsap.set("#motionSVG4", { scale: 0.9, autoAlpha: 1 });

animation = gsap.to("#motionSVG4", {
  scrollTrigger: {
    trigger: "#motionPath4",
    start: "top 120%",
    end: '+=2000px',
    scrub: 40,
    markers: true,
  },
  duration: 10,
  ease: "none",
  immediateRender: true,
  motionPath: {
    path: "#motionPath4",
    align: "#motionPath4",
    alignOrigin: [0.5, 0.5],
    autoRotate: 175,
  }
});

/// Animation Car-5 ///

var animation;

gsap.registerPlugin(MotionPathPlugin, ScrollTrigger);
gsap.set("#motionSVG5", { scale: 0.9, autoAlpha: 1 });

animation = gsap.to("#motionSVG5", {
  scrollTrigger: {
    trigger: "#motionPath5",
    start: "top 120%",
    end: '+=2000px',
    scrub: 40,
    markers: true,
  },
  duration: 10,
  ease: "none",
  immediateRender: true,
  motionPath: {
    path: "#motionPath5",
    align: "#motionPath5",
    alignOrigin: [0.5, 0.5],
    autoRotate: 175,
  }
});


/// Animation Car-6 ///

var animation;

gsap.registerPlugin(MotionPathPlugin, ScrollTrigger);
gsap.set("#motionSVG6", { scale: 0.9, autoAlpha: 1 });

animation = gsap.to("#motionSVG6", {
  scrollTrigger: {
    trigger: "#motionPath6",
    start: "top 105%",
    end: '+=2000px',
    scrub: 30,
    markers: true,
  },
  duration: 10,
  ease: "none",
  immediateRender: true,
  motionPath: {
    path: "#motionPath6",
    align: "#motionPath6",
    alignOrigin: [0.5, 0.5],
    autoRotate: 171,
  }
});


// Animation Car-7 ///

var animation;

gsap.registerPlugin(MotionPathPlugin, ScrollTrigger);
gsap.set("#motionSVG7", { scale: 0.9, autoAlpha: 1 });

animation = gsap.to("#motionSVG7", {
  scrollTrigger: {
    trigger: "#motionPath7",
    start: "top 70%",
    end: '+=2000px',
    scrub: 30,
    markers: true,
  },
  duration: 10,
  ease: "none",
  immediateRender: true,
  motionPath: {
    path: "#motionPath7",
    align: "#motionPath7",
    alignOrigin: [0.5, 0.5],
    autoRotate: 175,
  }
});

// Animation Car-8 ///

var animation;

gsap.registerPlugin(MotionPathPlugin, ScrollTrigger);
gsap.set("#motionSVG8", { scale: 0.9, autoAlpha: 1 });

animation = gsap.to("#motionSVG8", {
  scrollTrigger: {
    trigger: "#motionPath8",
    start: "top 105%",
    end: '+=2000px',
    scrub: 30,
    markers: true,
  },
  duration: 1,
  ease: "none",
  immediateRender: true,
  motionPath: {
    path: "#motionPath8",
    align: "#motionPath8",
    alignOrigin: [0.5, 0.5],
    autoRotate: 171,
  }
});