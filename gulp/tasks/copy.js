export const copy = () => {
    return app.gulp.src(app.path.src.files)
        .pipe(app.gulp.dest(app.path.build.files))
}

// import fileinclude from "gulp-file-include";

// export const html = () => {
//     return app.gulp.src(app.path.src.files)
//         .pipe(app.plugins.plumber(
//             app.plugins.notify.onError({
//                 title: "FILES",
//                 message: "Error: <%= error.message %>"
//             }))
//         )
//         .pipe(fileinclude())
//         .pipe(app.plugins.replace(/@img\//g, 'img/'))
//         .pipe(app.gulp.dest(app.path.build.files))
//         .pipe(app.plugins.browsersync.stream());
// }